﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	void Start () {
		
	}
	
	void Update () {
		if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D)){
			transform.position += new Vector3(Input.GetAxis("Horizontal") * 0.25f,0,0);
		}
		if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S)){
			transform.position += new Vector3(0,Input.GetAxis("Vertical") * 0.25f,0);
		}


	}
}
